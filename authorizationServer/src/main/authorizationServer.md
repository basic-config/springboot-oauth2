**● oauth2**
---
###● oauth2 role
1. resource owner : user
2. authorization server : 인증 API
3. resource server : access_token validation and scopes
4. client : applicetion

###● oauth2 grant_type
1. Authorization Code Grant Type : 권한 부여 코드 승인 타입
클라이언트가 다른 사용자 대신 특정 리소스에 접근을 요청할 떄 사용됩니다. 리스소 접근을 위한 사용자 명과 비밀번호, 권한 서버에 요청해서 받은 권한 코드를 함ㄲ 활용하여 리소스에 대한 엑세스 토큰을 받는 방식입니다.
2. Implicit Grant Type : 암시적 승인
권한 부여 코드 승인 타입과 다르게 권한 코드 교환 단계 없이 엑세스 토큰을 즉시 반환받아 이를 인증에 이용하는 방식입니다.
3. Resource Owner Password Credentials Grant Type : 리소스 소유자 암호 자격 증명 타입
클라이언트가 암호를 사용하여 엑세스 토큰에 대한 사용자의 자격 증명을 교환하는 방식식입니다.
4. Client Credentials Grant Type : 클라이언트 자격 증명 타입
클라이언트가 컨텍스트 외부에서 액세스 토큰을 얻어 특정 리소스에 접근을 요청할 때 사용하는 방식입니다.

참조 url : https://cheese10yun.github.io/oauth2/

https://daddyprogrammer.org/post/series/spring-boot-oauth2/

https://www.baeldung.com/rest-api-spring-oauth2-angular

### ● EnableAuthorizationServer
@EnableAuthorizationServer 서버는 토큰발급의 역할을 하는데 우리는 이곳에 필요에 따라 커스텀하게 기능을 변경할수 있다. 

tokenstore는 토큰을 저장하는 장소
authenticationManager 인증을 처리