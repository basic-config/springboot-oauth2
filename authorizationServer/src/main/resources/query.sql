CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(30) NOT NULL,
  `role` varchar(50) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

insert into users( username,password,name,role ) values( 'user', '$2a$10$K.PXhtCAysXwVpcWt0FGduN4cFy2ghlVRkZZx3dnc.n.ZWouYdV8.', '사용자', 'ROLE_ADMIN' );
