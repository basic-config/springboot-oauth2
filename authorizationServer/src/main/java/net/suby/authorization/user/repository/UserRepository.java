package net.suby.authorization.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.suby.authorization.user.domain.Users;

@Repository
public interface UserRepository extends JpaRepository<Users, String> {
}
