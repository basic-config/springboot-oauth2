package net.suby.authorization.user.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import net.suby.authorization.user.domain.Users;
import net.suby.authorization.user.repository.UserRepository;

@Service
@RequiredArgsConstructor
public class CustomedUserDetailsService  implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users = userRepository.findById(username).orElseThrow(() -> new UsernameNotFoundException("user is not exist")) ;

        List<GrantedAuthority> roles = new ArrayList<>();
        GrantedAuthority adminRole = new SimpleGrantedAuthority(users.getRole());
        roles.add(adminRole);

        return new User(users.getUsername(), users.getPassword(), roles);
    }


}
